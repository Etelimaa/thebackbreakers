﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    public enum CurrentPosition { Center, Left, Right, Up, Aft };

    private CurrentPosition currentPosition = CurrentPosition.Center;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetCameraRotation(float horizontalAxis, float verticalAxis) {

        if(horizontalAxis == -1 && currentPosition != CurrentPosition.Left) {
            Camera.current.transform.Rotate(0, -90, 0);
            currentPosition = CurrentPosition.Left;
        }else if(horizontalAxis == 1 && currentPosition != CurrentPosition.Right) {
            Camera.current.transform.Rotate(0, 90, 0);
            currentPosition = CurrentPosition.Right;
        }else if(verticalAxis == -1 && currentPosition != CurrentPosition.Aft) {
            Camera.current.transform.Rotate(0, 180, 0);
            currentPosition = CurrentPosition.Aft;
        }else if(verticalAxis == 1 && currentPosition != CurrentPosition.Up) {
            Camera.current.transform.Rotate(-90, 0, 0);
            currentPosition = CurrentPosition.Up;
        }else if (horizontalAxis == 0 && verticalAxis == 0) {
            if (currentPosition == CurrentPosition.Left) {
                Camera.current.transform.Rotate(0, 90, 0);
                currentPosition = CurrentPosition.Center;
            } else if (currentPosition == CurrentPosition.Right) {
                Camera.current.transform.Rotate(0, -90, 0);
                currentPosition = CurrentPosition.Center;
            } else if (currentPosition == CurrentPosition.Aft) {
                Camera.current.transform.Rotate(0, -180, 0);
                currentPosition = CurrentPosition.Center;
            } else if (currentPosition == CurrentPosition.Up) {
                Camera.current.transform.Rotate(90, 0, 0);
                currentPosition = CurrentPosition.Center;
            }
        }
    }
}
