﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ProjectileSoundManager : MonoBehaviour
{
    public AudioClip blastOff;
    public AudioClip impact;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        AudioSource.PlayClipAtPoint(blastOff, transform.position);
    }

    /// <summary>
    /// Plays projectile impact sound at location
    /// </summary>
    /// <param name="position"></param>
    public void PlayImpact(Vector3 position) {

        audioSource.pitch = Random.Range(0.1f, 5f);
        audioSource.volume = 0.5f;
        AudioSource.PlayClipAtPoint(impact, position);
    }
}
