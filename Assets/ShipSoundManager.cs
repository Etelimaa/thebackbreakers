﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ShipSoundManager : MonoBehaviour
{
    public AudioClip ShipExplosion;

    // Start is called before the first frame update
    void Start() {

    }

    /// <summary>
    /// Plays the ship explosion sound.
    /// </summary>
    /// <param name="position"></param>
    public void PlayShipExplosion(Vector3 position) {

        AudioSource.PlayClipAtPoint(ShipExplosion, position);
    }
}
