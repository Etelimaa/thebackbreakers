﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour
{
    [Tooltip("Turret Rotation Speed (not implemented)")]
    public float RotationSpeed = 2;

    public bool isRightSideTurret = false;

    private GameObject target;
    private Rigidbody targetBody;
    private Rigidbody ship;
    private Vector3 aimDirection;
    private Vector3 originalLocalRotation;
    private GameObject self;
    private WeaponController weaponController;
    private bool bTargetWithinWindow = false;
    private bool bTargetIsRightOfParent = false;
    
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Ship");
        ship = GetComponentInParent<Rigidbody>();
        weaponController = GetComponent<WeaponController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(target) { //TODO Targeting system for multiple targets, priority on targets closest to firing arc.
            
            targetBody = target.GetComponent<Rigidbody>();
            if(targetBody) {
                //Debug.Log("Targeting at" + targetBody.position.ToString());
                //Vector3 currentAngle = this.transform.rotation.eulerAngles;

                //Look at target, then determine wether it is within firing arc
                //TODO Polish targeting, crudely works. Improve firing arc detection!
                Vector3 currentForward = transform.forward;
                aimDirection = targetBody.position - transform.position;
                float shipYRotation = ship.transform.forward.x;
                if(shipYRotation < aimDirection.x) {
                    bTargetIsRightOfParent = true;
                } else {
                    bTargetIsRightOfParent = false;
                }
                float step = RotationSpeed * Time.deltaTime;
                aimDirection = Vector3.RotateTowards(transform.forward, aimDirection, step, 0.0f);
                Debug.DrawRay(transform.position, aimDirection, Color.red);

                transform.rotation = Quaternion.LookRotation(aimDirection);

                if(isRightSideTurret) {
                    
                    if(!bTargetIsRightOfParent || transform.localRotation.x < -60 || transform.localRotation.x > 60) {
                        bTargetWithinWindow = false;
                        aimDirection = ship.transform.right * 300 - transform.position;
                        aimDirection = Vector3.RotateTowards(currentForward, aimDirection, step, 0.0f);
                        transform.rotation = Quaternion.LookRotation(aimDirection);
                    }
                }else {
                    Debug.Log(transform.localRotation.y.ToString());
                    if (transform.localRotation.y < -150f || transform.localRotation.y > 30 || bTargetIsRightOfParent || transform.localRotation.x < -60 || transform.localRotation.x > 60) {
                        
                        bTargetWithinWindow = false;
                        aimDirection = ship.transform.right * -300 - transform.position;
                        aimDirection = Vector3.RotateTowards(currentForward, aimDirection, step, 0.0f);
                        transform.rotation = Quaternion.LookRotation(aimDirection);
                    }
                }
            }
        }

        if(weaponController.GetFireStatus() && bTargetWithinWindow) {
            weaponController.Fire(this.transform, "Mothership", targetBody.position);
        }

        bTargetWithinWindow = true;
    }
}
