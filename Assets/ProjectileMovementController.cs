﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovementController : MonoBehaviour
{
    [Tooltip("Projectile Speed")]
    public float muzzleVelocity = 300;

    [Tooltip("Damage dealt on impact")]
    public float damage = 10;

    [Tooltip("Maximum time the projectile stays in the game")]
    public float lifeTime = 5;

    private string ignoredTag = ""; //TODO ignored tags as array, once necessary

    private Rigidbody projectile;
    private ProjectileSoundManager soundManager;
    private float timer;
    private Vector3 targetPosition = Vector3.zero;
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        projectile = GetComponent<Rigidbody>();
        soundManager = GetComponent<ProjectileSoundManager>();
        timer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer + lifeTime < Time.time) {
            Destroy(this.gameObject);
        }

        //In case of targeted shells, explode near target. 
        if (targetPosition != Vector3.zero) { //TODO Random explosion distance, splash damage
            Vector3 distanceToTarget = targetPosition - projectile.position;
            if(distanceToTarget.magnitude < 1) {
                soundManager.PlayImpact(projectile.position);
                Destroy(this.gameObject);
            }
        }

        //In case of non-targeted shells, move forward. Targeted shells move (and look) towards target.
        if (targetPosition == Vector3.zero) {
            projectile.velocity = projectile.transform.forward * muzzleVelocity;
        } else  {
            direction = targetPosition - projectile.position;
            Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
            projectile.MoveRotation(rotation);
            direction.Normalize();
            projectile.MovePosition(projectile.position + direction * muzzleVelocity * Time.deltaTime);
        }
    }


    //Target response depending on collided tag. 
    void OnCollisionEnter(Collision collision) {
        string targetTag = collision.gameObject.tag;
        //Debug.Log(targetTag);
        if(targetTag.Equals(ignoredTag)) {
            Physics.IgnoreCollision(GetComponent<BoxCollider>(), collision.gameObject.GetComponent<BoxCollider>());
        }
        if (!targetTag.Equals(ignoredTag) && !targetTag.Equals("Projectile")) {
            soundManager.PlayImpact(projectile.position);
            if (targetTag.Equals("Ship")) {
                PlayerController controller = collision.gameObject.GetComponent<PlayerController>();
                controller.ApplyDamage(damage);
            }else if(targetTag.Equals("Mothership")) {
                MothershipAIController controller = collision.gameObject.GetComponent<MothershipAIController>();
                controller.ApplyDamage(damage);
            }
            Destroy(this.gameObject);
        }
        
    }

    /// <summary>
    /// Set a tag for the projectile to ignore. Limited to 1 tag. Optional.
    /// </summary>
    /// <param name="tag"></param>
    public void SetIgnoredTag(string tag) {
        ignoredTag = tag;
    }

    /// <summary>
    /// Set target point for the projectile. Optional.
    /// </summary>
    /// <param name="position"></param>
    public void SetTargetPosition(Vector3 position) {
        targetPosition = position;
    }
}
