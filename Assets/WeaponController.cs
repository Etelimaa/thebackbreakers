﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private GunBarrel m_AC;
    public GameObject AC_Projectile_Prefab;

    [Header("Reload time")]
    public float ReloadTime = 0.1f;

    private float timeFired;
    private bool bReadyToFire = true;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update() 
    {
        if(!bReadyToFire && Time.time > timeFired + ReloadTime) {
            bReadyToFire = true;
        }
    }

    /// <summary>
    /// Fire a projectile at location. Moves forward.
    /// </summary>
    /// <param name="Location"></param>
    /// <param name="ignoreTag"></param>
    public void Fire(Transform Location, string ignoreTag) {
        GameObject a = Instantiate(AC_Projectile_Prefab) as GameObject;
        a.GetComponent<ProjectileMovementController>().SetIgnoredTag(ignoreTag);
        a.transform.SetPositionAndRotation(Location.position, Location.rotation);
        ReloadTimer();
    }

    /// <summary>
    /// Fire a projectile at location. Moves toward targetPosition.
    /// </summary>
    /// <param name="Location"></param>
    /// <param name="ignoreTag"></param>
    /// <param name="targetPosition"></param>
    public void Fire(Transform Location, string ignoreTag, Vector3 targetPosition) {
        GameObject a = Instantiate(AC_Projectile_Prefab) as GameObject;
        a.GetComponent<ProjectileMovementController>().SetIgnoredTag(ignoreTag);
        a.transform.SetPositionAndRotation(Location.position, Location.rotation);
        a.GetComponent<ProjectileMovementController>().SetTargetPosition(targetPosition);
        ReloadTimer();
    }

    /// <summary>
    /// Starts the reload timer
    /// </summary>
    void ReloadTimer() {
        if (bReadyToFire) {
            bReadyToFire = false;
            timeFired = Time.time;
        }
    }

    /// <summary>
    /// Returns true if the weapon can be fired
    /// </summary>
    /// <returns></returns>
    public bool GetFireStatus() {
        return bReadyToFire;
    }
}
