﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Keyboard only for testing, not refined!
    public enum ControlMethod {Joystick, Keyboard };

    [Tooltip("Keyboard available for prototyping purposes")]
    public ControlMethod controlMethod = ControlMethod.Joystick;

    [Tooltip("Maximum speed of the ship")]
    public float MaxSpeed = 100;

    [Tooltip("Maximum speed on the pitch-axis")]
    public float PitchSpeed = 100;
    
    [Tooltip("Maximum speed on the roll-axis")]
    public float RollSpeed = 100;

    [Tooltip("Maximum speed on the yaw-axis")]
    public float YawSpeed = 100;

    [Tooltip("Ship health")]
    public float Health = 100;

    private ShipSoundManager soundManager;
    private WeaponController weaponController;
    private Rigidbody ship;
    private GunBarrel[] autocannons;
    private CameraController cameraController;
    private Vector3 eulerAngleVelocity;
    private float pitchValue;
    private float rollValue;
    private float yawValue;
    private float thrustModifier;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "Ship";
        ship = GetComponent<Rigidbody>();
        soundManager = GetComponent<ShipSoundManager>();
        autocannons = GetComponentsInChildren<GunBarrel>();
        weaponController = GetComponent<WeaponController>();
        cameraController = GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        //reset values every frame
        pitchValue = 0;
        rollValue = 0;
        yawValue = 0;

        if(Input.GetKeyDown(KeyCode.Space)) {
            //m_weaponController.Fire(m_AC_Right.transform);
            for (int i = 0; i < autocannons.Length; i++) {
                weaponController.Fire(autocannons[i].transform, "Ship");
            }
        }

        if(Input.GetKeyDown(KeyCode.F)) {
            if(thrustModifier > 0.19) {
                thrustModifier = thrustModifier - 0.2f;
            }else {
                thrustModifier = 0;
            }
        }

        if(Input.GetKeyDown(KeyCode.R)) {
            if (thrustModifier < 0.80) {
                thrustModifier = thrustModifier + 0.2f;
            } else {
                thrustModifier = 1;
            }
        }

        //do not factor in joystick input if keyboard input selected.
        switch (controlMethod)
        {
            case ControlMethod.Keyboard:
                rollValue = DetermineRollKeyboard();

                pitchValue = DeterminePitchKeyboard();

                yawValue = DetermineYawKeyboard();

                break;
            case ControlMethod.Joystick:
                rollValue = RollSpeed * -Input.GetAxis("Horizontal");

                pitchValue = PitchSpeed * Input.GetAxis("Vertical");

                yawValue = YawSpeed * Input.GetAxis("Yaw");

                thrustModifier = (Input.GetAxis("Thrust") + 1) / 2; //Thrustaxis is -1 to 1 as other axes.

                cameraController.SetCameraRotation(Input.GetAxis("Hat Horizontal"), Input.GetAxis("Hat Vertical"));

                if(Input.GetKey("joystick button 0") && weaponController.GetFireStatus()) {
                    for (int i = 0; i < autocannons.Length; i++) {
                        weaponController.Fire(autocannons[i].transform, "Ship");
                    }
                }

                break;
            default:
                Debug.Log("No input method selected");
                break;
        }



        eulerAngleVelocity = new Vector3(pitchValue, yawValue,  rollValue);
        Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);
        ship.MoveRotation(ship.rotation * deltaRotation);

        ship.velocity = transform.forward * MaxSpeed * thrustModifier;
    }

    private float DetermineYawKeyboard() {
        if (Input.GetKey(KeyCode.Period)) {
            return YawSpeed;
        } else if(Input.GetKey(KeyCode.Comma)) {
            return YawSpeed * -1;
        }else {
            return 0;
        }
    }

    private float DeterminePitchKeyboard() {

        if (Input.GetKey(KeyCode.UpArrow)) {
            return PitchSpeed;
        }else if (Input.GetKey(KeyCode.DownArrow)) {
            return PitchSpeed * -1;
        }else {
            return 0;
        }
    }

    private float DetermineRollKeyboard()
    {
        if (Input.GetKey(KeyCode.RightArrow)) {
           return RollSpeed * -1;
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            return RollSpeed;
        }else {
            return 0;
        }
    }

    /// <summary>
    ///  Applies damage to the ship, diminishing health. If health drops below 1hp, ship explodes
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(float damage) {
        Debug.Log("DAMAGES!");
        Health -= damage;
        if (Health < 1) {
            soundManager.PlayShipExplosion(ship.position);
            Destroy(this.gameObject);
        }
    }
}
