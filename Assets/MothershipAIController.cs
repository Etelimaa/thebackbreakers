﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothershipAIController : MonoBehaviour
{

    [Tooltip("Motheship health")]
    public float Health = 400;

    private ShipSoundManager soundManager;
    private Rigidbody ship;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "Mothership";
        soundManager = GetComponent<ShipSoundManager>();
        ship = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Applies damage to the ship, diminishing health. If health drops below 1hp, ship explodes
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(float damage) {
        Health -= damage;
        Debug.Log("Mothership Health: " + Health.ToString());
        if (Health < 1) {
            if (soundManager && ship)
                soundManager.PlayShipExplosion(ship.position);

            Destroy(this.gameObject);
        }
    }
}
